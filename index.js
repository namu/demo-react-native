// importare una libreria per i componenti
import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/header';
import EventList from './src/eventList';
import ViewDetail from './src/viewDetail';
import { StackNavigator } from 'react-navigation';

export const Navigator = StackNavigator({
  EventList: {
    screen: EventList
  },
  ViewDetail : {
    screen: ViewDetail
  }
}, {
    navigationOptions: {
      initialRouteName: 'EventList'
    }
  });


const App = () => {
  return (
    <Navigator/>
  );
};

// renderizzare il componente 

AppRegistry.registerComponent('tcs', () => App);