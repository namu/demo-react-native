import React, { Component} from 'react';
import {WebView} from 'react-native';

class ViewDetail extends Component {

  static navigationOptions = ({ navigation }) => {
    const event = navigation.getParam('event');
    return {
      title: event.title.rendered,
    };
  };
  
  render(){
    const { navigation } = this.props;
    const event = navigation.getParam('event');
    console.log(event);
    return (
      <WebView source={{ uri: event.link}} />
    );
  }
}

export default ViewDetail;