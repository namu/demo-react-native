// importare una libreria per i componenti
import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => {

  const { title } = props;
  const { textStyle, viewStyle } = styles;
  return (
    <View style = { viewStyle }>
      <Text style = { textStyle }>{title}</Text>
    </View>
  );
};

const styles = {
  textStyle: {
    fontSize: 20
  },
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 15,
    height: 60,
    backgroundColor: '#F8F8F8',
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.2
  }
};

export default Header;