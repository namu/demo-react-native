import React, { Component } from 'react';
import { Text, View, Image, TouchableHighlight } from 'react-native';
import axios from 'axios';
import HTMLView from 'react-native-htmlview';
import { withNavigation } from 'react-navigation';
class EventCard extends Component {

  constructor (props) {
    super(props)
    this.state = {
      image: ''
    }
    this.getEventCover(props.event._links['wp:featuredmedia'][0].href);
    this._onPressCard = this._onPressCard.bind(this);
  }

  _onPressCard = () => {
    this.props.navigation.navigate('ViewDetail', {
      event: this.props.event
    });
  }

  getEventCover(url) {
    axios.get(url).then(response => this.setState({ image: response.data.media_details.sizes.post.source_url }))
  }

  render() {
    const he = require('he');
    return (
      <TouchableHighlight onPress={this._onPressCard}>
      <View style={styles.cardStyle}>
          <View style={styles.thumbnailContainerStyle}>
            <Image
              style={styles.thumbnailStyle}
              source={{ uri: this.state.image }}
            />
          </View>
          <View style={styles.headerContentStyle}>
            <Text style={styles.headerTextStyle}>{he.decode(this.props.event.title.rendered)}</Text>
          </View>
          <HTMLView
            value={this.props.event.content.rendered}
          />
      </View>
      </TouchableHighlight>
    );
  }
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18
  },
  descriptionContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  descriptionTextStyle: {
    p: {
      fontSize: 13,
      padding: 0,
      margin: 0
    }
  },
  thumbnailStyle: {
    height: 200,
    flex: 1,
    width: null
  },
  thumbnailContainerStyle: {
    flexDirection: 'row',
  },
  cardStyle: {
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    padding: 5,
    elevation: 1,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10
  }
};

export default withNavigation(EventCard);
