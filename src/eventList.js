import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native';
import EventCard from './eventCard.js';
import axios from 'axios';

class EventList extends Component {

    static navigationOptions = {
      title: 'Torino Coding Society'
    };  

  state = { events: [] };

  componentWillMount() {
    axios.get('https://torinocodingsociety.it/wp-json/wp/v2/events')
      .then(response => this.setState({events: response.data }));

  }
  renderEventList(){
    return this.state.events.map(event =>
      <EventCard key={event.id} event={event} />
    );
  }

  render() {
    console.log(this.state.events);
    return (
      <ScrollView>
        { this.renderEventList()}
      </ScrollView>
    );
  }
}

export default EventList;